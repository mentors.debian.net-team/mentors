#!/usr/bin/env bash
# Copyright 2020 Baptiste BEAUPLAT
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

#
# This script enables you to import the entire Debian archive to debexpo. It was
# written with the intent to test the import process extensively (and mostly
# because it was fun).
#
# run with a gpg key id in argument to sign the changes and dsc:
#
# ./import-all-the-things.sh 0xMYKEYID
#

db="import-all-the-things"
all_pkg_list="${db}/packages.list"
done_pkg_list="${db}/packages.done"
results="${db}/results"
workdir="${db}/workdir"
failed="${results}/failed"
passed="${results}/passed"
dist="${2:-sid}"
key="${1:?missing gpg key argument}"
quit="false"

init() {
    pushd "$(dirname -- "$(readlink -f -- "$0")")" > /dev/null
    mkdir -p "${db}" "${failed}" "${passed}"
    trap "quit=true" SIGINT
}

download_pkg_list() {
    echo 'downloading package list'
    curl -s \
        "http://deb.debian.org/debian/dists/${dist}/main/source/Sources.gz" |
        zcat |
        grep-dctrl -s Package,Version - |
        sed -z -e 's/Package: //g' -e 's/\nVersion://g' |
        sed '/^$/d' > "${all_pkg_list}"
}

load_pkg_list() {
    echo 'loading packages'
    [[ ! -f "${all_pkg_list}" ]] && download_pkg_list
    [[ ! -f "${done_pkg_list}" ]] && touch "${done_pkg_list}"

    mapfile -t packages < <(sort "${all_pkg_list}" "${done_pkg_list}" | uniq -u)
}

prepare() {
    cleanup
    name="${package/ */}"
    version="${package/* /}"
    changes="${name}_${version}_source.changes"
}

download() {
    echo "${name}-${version}: downloading"
    debsnap --destdir "${workdir}" "${name}" "${version}"

    skip_if_failed "download failed"
}

extract() {
    echo "${name}-${version}: extracting"

    # We try to be smart about extracting the source package. Only the debian
    # packages files are required to build the .changes.
    if ls "${workdir}/"*.debian.tar* > /dev/null 2>&1; then
        mkdir -p "${workdir}/extracted"
        tar -xf "${workdir}/"*.debian.tar* -C "${workdir}/extracted" \
            > /dev/null 2>&1
    else
        dpkg-source -x "${workdir}/"*.dsc "${workdir}/extracted" > /dev/null \
        2>&1
    fi

    skip_if_failed "extract failed"
}

gen_changes() {
    pushd "${workdir}/extracted" > /dev/null
    dpkg-genchanges -q -sa > "../${changes}"
    popd > /dev/null

    skip_if_failed "changes failed"
}

sign() {
    debsign --re-sign -k "${key}" "${workdir}/"*.dsc > /dev/null 2>&1
    debsign --re-sign -k "${key}" "${workdir}/"*.changes > /dev/null 2>&1

    skip_if_failed "signature failed"
}

import() {
    echo "${name}-${version}: importing"
    output=$(python ../manage.py import "${workdir}/${changes}" 2>&1)
    result="$?"
}

cleanup() {
    rm -rf "${workdir}"
}

mark_as_done() {
    if [[ "${result}" -eq 0 ]]; then
        echo "$output" > "${passed}/${name}-${version}"
        echo "${name}-${version}: PASS"
    else
        echo "$output" > "${failed}/${name}-${version}"
        echo "${name}-${version}: FAIL"
    fi

    echo "${name} ${version}" >> "${done_pkg_list}"
}

skip_if_failed() {
    if [[ "$?" -ne 0 ]]; then
        echo "${name}-${version}: SKIP (${1})"
        false
    else
        true
    fi
}

init
load_pkg_list

for package in "${packages[@]}"; do
    $quit && break
    prepare
    download || continue
    extract || continue
    gen_changes || continue
    sign || continue
    import
    cleanup
    mark_as_done
done
