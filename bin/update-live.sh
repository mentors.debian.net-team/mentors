#!/bin/bash

set -eu

homedir="/home/expo/debexpo"
flagfile="/tmp/debexpo.maint"

if [ ! -O "$homedir/debexpo/settings/prod.py" ]; then
    echo "User not matching, trying to re-exec under the correct user..."
    exec sudo -u expo "$0" "$@"
fi

set -x

touch "$flagfile"
trap 'rm -v "$flagfile"' EXIT

cd "$homedir"

# needed by systemctl
XDG_RUNTIME_DIR="/run/user/$UID"
export XDG_RUNTIME_DIR

systemctl --user stop debexpo.service
git pull --ff-only
./manage.py migrate
./manage.py collectstatic --no-input --clear
./manage.py compilemessages
touch debexpo/wsgi.py
systemctl --user restart debexpo.service
