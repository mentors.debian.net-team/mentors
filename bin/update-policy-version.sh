#!/bin/bash
# Copyright 2024 Baptiste Beauplat <lyknode@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Packages dependencies:
# sudo apt install libmce-perl libwww-mechanize-perl libhtml-tokeparser-simple-perl

set -e

lintian_cache_dir="${HOME}/.cache/salsa/lintian"
lintian_repo="https://salsa.debian.org/lintian/lintian.git"
lintian_branch="origin/master"
policy_data_file="../share/lintian/policy-data"

init() {
    pushd "$(dirname -- "$(readlink -f -- "$0")")" > /dev/null
    mkdir -p "${lintian_cache_dir}"
}

update_lintian_repo() {
    if [[ ! -d "${lintian_cache_dir}/.git" ]]; then
        git -C "${lintian_cache_dir}" clone "${lintian_repo}" .
    else
        git -C "${lintian_cache_dir}" reset --hard "${lintian_branch}"
        git -C "${lintian_cache_dir}" clean -fdx
        git -C "${lintian_cache_dir}" pull
    fi
}

update_policy_data() {
    pushd "${lintian_cache_dir}/private" > /dev/null
    ./refresh-data policy
    popd
}

get_policy_data() {
    cp "${lintian_cache_dir}/data/debian-policy/releases.json" \
        "${policy_data_file}"
}

init
update_lintian_repo
update_policy_data
get_policy_data
