# Mentors

mentors.debian.net debexpo's deployment tools and docs

## Lintian policy data

In order for lintian not to lag behind unknown version of the policy, we hold a
local copy of lintian releases.json file.

It can be updated using the script:

```bash
./bin/update-policy-version.sh
```

To setup the local copy on mentors, run once:

```bash
su - expo
git clone https://salsa.debian.org/mentors.debian.net-team/mentors.git
cd mentors
mkdir -p ~/.lintian/vendors/debian/main/data/debian-policy
ln -s ~/mentors/share/lintian/policy-data ~/.lintian/vendors/debian/main/data/debian-policy/releases.json
```

To refresh the repository after an update, just run:

```bash
su - expo
cd mentors
git pull
```
